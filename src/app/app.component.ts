import { Component } from '@angular/core';
import { Productos } from './common/domain/productos';
import {ModalService} from './common/modal-Service/modal.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

constructor(private modalService: ModalService ){

}

  seleccionProducto: Productos = new Productos();

  productosArray: Productos[] = [
    { id: 1, nombre: "Pumba #1", marca: "Tersil", material: "plástico" },
    { id: 2, nombre: "Botellin Flip Top", marca: "Cubasa", material: "plástico" },
    { id: 3, nombre: "Cesta calada Danubio", marca: "Peyo", material: "plástico" },
    { id: 4, nombre: "Bote ergo lechero", marca: "Cuplasa", material: "plástico" },
    { id: 5, nombre: "Rallador cuadrado", marca: "Yuliana", material: "Acero Inoxidable" }
  ];

  agregarOEditar() {
    if(this.seleccionProducto.id === 0){
      if(this.seleccionProducto.marca === '' || this.seleccionProducto.marca === null || this.seleccionProducto.marca === undefined ||
      this.seleccionProducto.material === '' || this.seleccionProducto.material === null || this.seleccionProducto.material === undefined||
      this.seleccionProducto.nombre === '' || this.seleccionProducto.nombre === null || this.seleccionProducto.nombre === undefined){
        console.log('no hay datos',this.seleccionProducto);
      }else{
        this.seleccionProducto.id = this.productosArray.length + 1;
        this.productosArray.push(this.seleccionProducto);
      }
    }

    this.seleccionProducto = new Productos();
  }

  editar(producto: Productos) {
    this.seleccionProducto = producto;
  }

  borrar(){
    this.productosArray = this.productosArray.filter(x => x != this.seleccionProducto);
    this.seleccionProducto = new Productos();
    this.modalService.close('prueba');
  }

  modal(modal:string){
    this.modalService.open(modal);
  }

  closeModal(modal:string){
    this.modalService.close(modal);
  }

}
